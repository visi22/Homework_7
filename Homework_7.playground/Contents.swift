//: Описать несколько структур – любой легковой автомобиль и любой грузовик. Структуры должны содержать марку авто, год выпуска, объем багажника/кузова

enum Actions {
    case startEngine
    case stopEngine
    case openWindows
    case closeWindows
    case load
    case upload
    case trunk
    case body
}


struct Auto: Hashable {
    var brand: String
    var yearOfRelease: UInt
    var volumeOfTrunk: Double
    var engineIsRunning: Bool
    var openWindows: Bool
    var cargo : Double
    var filledTrunkVolume: Double {
        get {
            if cargo > volumeOfTrunk {
                print("The cargo exceeds the trunk volume")
                return -1
            } else {
                return cargo * 100 / volumeOfTrunk
            }
        }
    }
    
    mutating func change(actions: Actions){
        switch actions {
        case .startEngine:
            engineIsRunning = true
            print("The engine is running")
        case .stopEngine:
            engineIsRunning = false
            print("The engine is stopped")
        case .openWindows:
            openWindows = true
            print("The windows are open")
        case .closeWindows:
            openWindows = false
            print("The windows are closed")
        case .load:
            if filledTrunkVolume != -1 {
                print("Loading the cargo into the trunk is completed. The cargo occupies \(filledTrunkVolume) percent of the trunk volume")
            }
        case .upload:
            cargo = 0
            print("cargo unloading is completed")
        default: break
        }
    }
}


struct Truck: Hashable {
    var brand: String
    var yearOfRelease: UInt
    var volumeOfBody: Double
    var engineIsRunning: Bool
    var openWindows: Bool
    var cargo : Double
    var filledBodyVolume: Double {
        get {
            if cargo > volumeOfBody {
            print("The cargo exceeds the body volume! You will not be able to load!")
            return -1
            } else {
                return cargo * 100 / volumeOfBody
            }
        }
    }
    
    mutating func change(actions: Actions){
        switch actions {
        case .startEngine:
            engineIsRunning = true
            print("The engine is running")
        case .stopEngine:
            engineIsRunning = false
            print("The engine is stopped")
        case .openWindows:
            openWindows = true
            print("The windows are open")
        case .closeWindows:
            openWindows = false
            print("The windows are closed")
        case .load:
            if filledBodyVolume != -1 {
                print("Loading the cargo into the body is completed. The cargo occupies \(filledBodyVolume) percent of the Body volume")
            }
        case .upload:
            cargo = 0
            print("cargo unloading is completed")
        default: break
        }
    }
}



var firstTruck = Truck(brand: "Volvo", yearOfRelease: 2010, volumeOfBody: 1000, engineIsRunning: false, openWindows: true, cargo: 100)

firstTruck.change(actions: .startEngine)
firstTruck.change(actions: .load)
firstTruck.change(actions: .upload)

var firstAuto = Auto(brand: "Porsche", yearOfRelease: 2022, volumeOfTrunk: 100, engineIsRunning: false, openWindows: false, cargo: 20)
firstAuto.change(actions: .load)

var dict = [firstTruck: "firstTruck", firstAuto: "firstAuto"] as [AnyHashable : String]

//: Capture list

class Animal {
    
    let name: String
    let power: Int?
    
    lazy var about: () -> String = {
        [unowned self] in
        if let power = self.power {
            return "\(self.name) has \(power) power points"
        } else {
            return "\(self.name) has nil power points"
        }
    }

    init(name: String, power: Int? = nil) {
        self.name = name
        self.power = power
    }
    
    deinit {
        print("\(name) освобождается")
    }
}

var cat: Animal? = Animal(name: "Cat", power: 10)
print(cat!.about())
cat = nil

// Без использования [unowned self] класс Animal приводил бы к созданию цикла сильных ссылок между экземпляром и замыканием.А именно: свойство about экземпляра держит сильную ссылку на его замыкание, но так как замыкание ссылается на self внутри своего тела (self.name, self.power), оно захватывает self, то есть замыкание держит сильную ссылку обратно на экземпляр Animal, поэтому ни экземпляр ни замыкание не освобождались. При использовании [unowned self] захват self является бесхозной ссылкой и уже не поддерживает сильной связи с экземпляром.

//: Набрать код который на белом скрине понять в чем там проблема и решить эту проблему

class Car {
    weak var driver: Man?
    
    deinit {
        print("машина удалена из памяти")
    }
}

class Man {
    var myCar: Car?
    
    deinit {
        print("мужчина удалена из памяти")
    }
}


var car: Car? = Car()
var man: Man? = Man()

car?.driver = man
man?.myCar = car

car = nil
man = nil

//: У нас есть класс мужчины и его паспорта.

class ManAnother {
    var pasport: Passport?
    
    deinit {
        print("мужчина удален из памяти")
    }
}

class Passport {
    unowned let manAnother: ManAnother
    
    init(manAnother: ManAnother) {
        self.manAnother = manAnother
    }
    
    deinit {
        print("паспорт удален из памяти")
    }
}

var manAnother: ManAnother? = ManAnother()
var passport: Passport? = Passport(manAnother: manAnother!)
manAnother?.pasport = passport
passport = nil
manAnother = nil
